#!/usr/bin/env bash
# This script highlights typos

path="$1"

egrep -n " : " "$path"              # " : "
egrep -n "[^ ][({]" "$path"         # [word]( or [word]{

# does not work
# egrep -n "[})][^ ]" input.md | grep -v "[})][.]"
# egrep -n "[})][^ ]" input.md | grep -v "[})][.]"   no hace bien el segundo filtrado con -v
# egrep -n "[})][^ ]" input.md                       falso positivo con los correct sentences

egrep -n "[})][^ .\"]" "$path"      # )[word] or }[word] or )" or }" or )[trailing_space] or }[trailing_space]


# primero filtramos todas las lineas que no termien en ." (ie las sentences correctas son ignoradas)
# luego, mostramos todas las lineas que no terminan en punto
egrep -n -v "\.\"$" "$path" | egrep "[^.]$"
